<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('post/{post}','PostController@show');

Route::get('/','PostController@index');

Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');

Route::post('/post/edit','PostController@edit');

//Comments
Route::post('/comment','CommentController@store');

//Category
Route::get('/category','CategoryController@index');
Route::get('/categoryadd','CategoryController@create');
Route::post('/categoryadd','CategoryController@store');
Route::get('/category/delete/{post}','CategoryController@destroy');

Auth::routes();

Route::get('/upload','PostController@create');
Route::post('/upload','PostController@store');


Route::get('/home', 'HomeController@index')->name('home');

//Admin tavle
Route::get('admin-login','Auth\AdminLoginController@showLoginForm');

Route::post('admin-login','Auth\AdminLoginController@login');

Route::get('/backend','AdminController@index');

