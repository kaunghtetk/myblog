<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }

    public function index()
    {
        //
        $posts=Post::all();
        return view('blog',compact('posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(request(),[
            'title'=>'required|min:5',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg,max:2048',
            'body'=>'required|min:10'
        ]);

        $imageName = time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'),$imageName);

        $fullImg = '/images/'.$imageName;

        Post::create([
            'title'=> request('title'),
            'category_id'=>1,
            'photo'=>$fullImg,
            'body'=>request('body'),
            'user_id'=>auth()->id()
        ]);

        // dd($request);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
        //dd($post);
        return view('blogpost',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categories= Category::all();
        $post = Post::find($id);
        return view('posts.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate(request(),[
            'title'=>'required|min:5',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg,max:2048',
            'body'=>'required|min:10'
        ]);

        $imageName = time().'.'.request()->photo->getClientOriginalExtension();
        request()->photo->move(public_path('images'),$imageName);

        $fullImg = '/images/'.$imageName;

        $id=request('editid');

        $post=Post::find($id);
        $post->title=request('title');
        $post->category_id=request('category_id');
        $post->body=request('body');
        $post->user_id=auth()->id();
        $post->save();

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        // dd($post);
        foreach($post->comments as $comment):
            $comment->delete();
        endforeach;
        $post->delete();
        return redirect('/')->with('msg','asdtff');

    }
}
