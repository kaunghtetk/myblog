<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    //

    public function index()
    {
    	
    }
   public function create()
   {
   		
   }

   public function store(Request $request)
   {
   	$this->validate(request(),[
   		'postid'=>'required',
   		'comment'=>'required|min:5'

   	]);

   	Comment::create([
   		'post_id'=>request('postid'),
   		'body'=>request('comment'),
   		'user_id'=>auth()->id()

   	]);
   	return back();

   }

}
