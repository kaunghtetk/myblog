@extends('layouts.template')

@section('content')

<div class="col-md-8 my-4">
	<a href="/categoryadd" class="btn btn-primary btn-lg float-right">Add Category</a><br><br><br>
	<table class="table table-secondary table-hover table-bordered" style="text-align: center;">
		<thead>
			<tr>
				<th>
					No.
				</th>
				<th>
					Name
				</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		@foreach($categories as $category)
		<tbody>
			
			<tr>
				
				<td>
					{{$category->id}}
				</td>

				<td>
					{{$category->category_name}}
				</td>
				<td>
					<a href="" class="btn btn-outline-success">Edit</a>
				</td>
				<td>
					<a href="category/delete/{{$category->id}}" class="btn btn-outline-warning">Delete</a>
				</td>
				
			</tr>
		
		</tbody>
		@endforeach
	</table>
</div>





@endsection(content)

