@extends('layouts.template')

@section('content')


<div class="col-md-8">
	<div class="py-3">
		<a href="\category" class="btn btn-dark btn-lg">Go Back</a>
	</div>
	<h1 class="text-center">Enter a new category</h1>
	<form class="px-5" method="POST">
		@csrf
		<div class="form-group">
			<input type="text" name="name" class="form-control" placeholder="Enter a name">
		</div>
		<div class="form-group">
			<button class="btn btn-success" name="save">Save</button>
		</div>
	</form>
</div>


@endsection('content')