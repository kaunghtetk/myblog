@extends('layouts.template')

@section('content')
  <!-- Post Content Column -->
        <div class="col-md-8">

          <!-- Title -->
          <h1 class="mt-4">{{ $post->title}}</h1>

          <!-- Author -->
          <p class="lead">
            by
            @if(Auth::check()&& ($post->user_id == auth()->id()))
            <a href="#">{{ $post->user->name}}</a>
            <a href="/post/edit/{{$post->id}}" class="btn btn-outline-info">Edit</a>
            <a href="/post/delete/{{$post->id}}" class="btn btn-outline-danger">Delete</a>
            @endif
          </p>

          <hr>

          <!-- Date/Time -->
          <p>Posted on January 1, 2018 at 12:00 PM</p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="{{ $post->photo}}" alt="">

          <hr>

          <!-- Post Content -->
         {!!$post->body!!}

          <hr>

          <!-- Comments Form -->
          @if(!Auth::check())
          <div class="alert alert-danger">You Must Login First!</div>
          @endif
          <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              <form method="post" action="/comment">
                @csrf
                <input type="hidden" name="postid" value="{{$post->id}}">
                <div class="form-group"> 
                  <textarea class="form-control" rows="3" name="comment"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>

          <!-- Single Comment -->
          @foreach($post->comments as $comment)
          <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
              <h5 class="mt-0">{{$comment->user->name}}</h5>
              {{$comment->body}}

            </div>
          </div>
          @endforeach
          <!-- Comment with nested comments -->
        </div>
@endsection()