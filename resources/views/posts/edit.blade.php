@extends('layouts.template')

@section('content')
	
		<div class="col-md-8">
			@if(count($errors))
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
<div class="col-md-8">
	<form method="POST" action="/upload" enctype="multipart/form-data">
		@csrf
			<div class="form-group">
				<label>Post Title:</label>
				<input type="text" name="title" class="form-control" value="{{$post->title}}">
			</div>
			<div class="form-group">
				<label>Photo</label>
				<input type="file" name="photo" class="form-control-file">
				<img src="{{$post->photo}}" width="100px">
			</div>
			<div class="form-group">
				<label>Choose Category:</label>
				<select name="category_id" class="form-control">
					@foreach($categories as $category)
						<option value="{{$category->id}}"
							<?php 
								if($category->id == $post->category_id) {echo "selected"; }
							?>
							>
							{{$category->category_name}}					
						</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Post Body:</label>
				<textarea name="body" class="form-control" id="summernote">{!!$post->body!!}</textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="btnsubmit" class="btn btn-info" value="Upload">
			</div>
	</form>
</div>
@endsection
